import React from "react";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import 'react-native-gesture-handler';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import reducer from './Reducers/GeldugReducer';

const store = createStore(reducer, applyMiddleware(thunk))

import AboutScreen from './About';
import DetailScreen from './homedetail';
import HomeScreen from './home';
import LoginScreen from './login';

import Detail from './detail';

const TabsStack = createBottomTabNavigator();
const RootStack = createStackNavigator();

const AboutStack = createStackNavigator();
const HomeStack = createStackNavigator();
const DetailStack = createStackNavigator();

const iconTab = ({ route }) => {
    return ({
        tabBarIcon: ({ focused, color, size }) => {
            if(focused===true){
                color='#337ab7';
            }
            if (route.name == 'DetailScreen') {
                return <FontAwesome5 name="cloud" size={size} color={color} solid />
            }else if (route.name == 'HomeScreen') {
                return <FontAwesome5 name='search' size={size} color={color} solid />
            } else if (route.name == 'AboutScreen') {
                return <FontAwesome5 name="user-circle" size={size} color={color} solid />
            }
        },
    });
}

const TabsStackScreen = ({navigation}) => (
  <TabsStack.Navigator screenOptions={iconTab} >
      <TabsStack.Screen name='DetailScreen' component={DetailScreen}
          options={{
              tabBarLabel: 'Home',
          }} />
      <TabsStack.Screen name='HomeScreen' component={HomeScreen}
          options={{
              title: 'Search'
          }} />
      <TabsStack.Screen name='AboutScreen' component={AboutScreen}
          options={{
              title: 'About Us'
          }}
      />
  </TabsStack.Navigator>
);

const Stack = createStackNavigator();
const TabsStackScreenDetail = () => (
    <Stack.Navigator initialRouteName="Home" >
        <Stack.Screen name='Detail' component={Detail} options={{ headerShown: false }} />
        <Stack.Screen name='Home' component={TabsStackScreen} options={{ headerShown: false }}/>
        
    </Stack.Navigator>
);

const RootStackScreen = () => (
  <RootStack.Navigator>
      <RootStack.Screen name='LoginScreen' component={LoginScreen} options={{ headerShown: false }} />
      <RootStack.Screen name='HomeScreen' component={TabsStackScreenDetail} options={{ headerShown: false }} />
  </RootStack.Navigator>
);

export default () => (
    <Provider store={store}>
        <NavigationContainer>
            {/* <TabsStackScreen /> */}
            {/* <TabsStackScreenDetail/> */}
            <RootStackScreen/>
        </NavigationContainer>
    </Provider>
)