import {
    FETCH_CITIES_REQUEST,
    FETCH_CITIES_SUCCESS,
    FETCH_CITIES_ERROR,

    FETCH_DETAIL_REQUEST,
    FETCH_DETAIL_SUCCESS,
    FETCH_DETAIL_ERROR,

    FETCH_DETAILDEF_REQUEST,
    FETCH_DETAILDEF_SUCCESS,
    FETCH_DETAILDEF_ERROR,

} from './Types';

const initialState = {

    citiesIsLoading: false,
    citiesHttpStatus: 200,
    citiesHttpMsg: '',
    citiesData: [],

    detailsIsLoading: false,
    detailsHttpStatus: 200,
    detailsHttpMsg: '',
    detailsData: [],

    detaildefsIsLoading: false,
    detaildefsHttpStatus: 200,
    detaildefsHttpMsg: '',
    detaildefsData: [],

}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CITIES_REQUEST:
            return {
                ...state,
                citiesIsLoading: true
            };
        case FETCH_CITIES_SUCCESS:
            return {
                ...state,
                citiesIsLoading: false,
                citiesData: action.data

            };
        case FETCH_CITIES_ERROR:
            return {
                ...state,
                citiesIsLoading: false,
                citiesData: [],
                citiesHttpStatus: action.status,
                citiesHttpMsg: action.msg
            };
            //DETAIL
        case FETCH_DETAIL_REQUEST:
            return {
                ...state,
                detailsIsLoading: true
            };

        case FETCH_DETAIL_SUCCESS:
            return {
                ...state,
                detailsIsLoading: false,
                detailsData: action.data

            };
        case FETCH_DETAIL_ERROR:
            return {
                ...state,
                detailsIsLoading: false,
                detailsData: [],
                detailsHttpStatus: action.status,
                detailsHttpMsg: action.msg
            };
            //DETAILDEF
        case FETCH_DETAILDEF_REQUEST:
            return {
                ...state,
                detaildefsIsLoading: true
            };

        case FETCH_DETAILDEF_SUCCESS:
            return {
                ...state,
                detaildefsIsLoading: false,
                detaildefsData: action.data

            };
        case FETCH_DETAILDEF_ERROR:
            return {
                ...state,
                detaildefsIsLoading: false,
                detaildefsData: [],
                detaildefsHttpStatus: action.status,
                detaildefsHttpMsg: action.msg
            };

        default: return state;
    }
    return state;
}

export default reducer