import {
    FETCH_CITIES_REQUEST,
    FETCH_CITIES_SUCCESS,
    FETCH_CITIES_ERROR,

    FETCH_DETAIL_REQUEST,
    FETCH_DETAIL_SUCCESS,
    FETCH_DETAIL_ERROR,

    FETCH_DETAILDEF_REQUEST,
    FETCH_DETAILDEF_SUCCESS,
    FETCH_DETAILDEF_ERROR,

} from '../Reducers/Types';

export const ambilCityAPI = (dispatch, param) => {
    dispatch({
        type: FETCH_CITIES_REQUEST
    })

    const callbackOk = (json) => {
        // console.log("call back ok", json)
        dispatch({
            type: FETCH_CITIES_SUCCESS,
            data: json
        })
    }

    const callbacErr = (error) => {
        // console.log("call back error", error)
        dispatch({
            type: FETCH_CITIES_ERROR,
            status: error.response.status,
            msg: error.response.statusText
        })
    }

    url = `https://www.metaweather.com/api/location/search/?query=${param}`
    console.log(url)
    fetchAPI(url, callbackOk, callbacErr)
}

export const ambilDetailAPI = (dispatch, param) => {
    // console.log("ini detail API----------------", param);
    dispatch({
        type: FETCH_DETAIL_REQUEST
    })

    const callbackOk = (json) => {
        // console.log("call back ok", json)
        dispatch({
            type: FETCH_DETAIL_SUCCESS,
            data: json
        })
    }

    const callbacErr = (error) => {
        // console.log("call back error", error)
        dispatch({
            type: FETCH_DETAIL_ERROR,
            status: error.response.status,
            msg: error.response.statusText
        })
    }

    url = `https://www.metaweather.com/api/location/${param}`
    console.log(url)
    fetchAPI(url, callbackOk, callbacErr)
}

export const ambilDetailDefAPI = (dispatch, param) => {
    // console.log("ini detail API----------------", param);
    dispatch({
        type: FETCH_DETAILDEF_REQUEST
    })

    const callbackOk = (json) => {
        // console.log("call back ok", json)
        dispatch({
            type: FETCH_DETAILDEF_SUCCESS,
            data: json
        })
    }

    const callbacErr = (error) => {
        // console.log("call back error", error)
        dispatch({
            type: FETCH_DETAILDEF_ERROR,
            status: error.response.status,
            msg: error.response.statusText
        })
    }

    url = `https://www.metaweather.com/api/location/${param}`
    console.log(url)
    fetchAPI(url, callbackOk, callbacErr)
}

export const fetchAPI = async (url, callbackOk, callbackErr) => {
    // console.log("jesson ==== "+url);
    const response = await fetch(url)
        .then((response) => {
            let json = response.json()
            return json
        })
        .then((data) => {
            typeof callbackOk === 'function' && callbackOk(data)
            // console.log("json >>>>>>>>> "+data)
        })
        .catch((error) => {
            typeof callbackErr === 'function' && callbackErr(error)
        });
};

