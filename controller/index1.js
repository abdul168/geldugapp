import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import 'react-native-gesture-handler';

import AboutScreen from './About';
import DetailScreen from './detail';
import HomeScreen from './home';

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import reducer from './Reducers/GeldugReducer';

const store = createStore(reducer, applyMiddleware(thunk))

const Stack = createStackNavigator();
const TabsStackScreen = () => (
  <Stack.Navigator initialRouteName="Home" >
    <Stack.Screen name='About' component={AboutScreen} options={{ headerShown: false }} />
    <Stack.Screen name='Detail' component={DetailScreen} options={{ headerShown: false }} />
    <Stack.Screen name='Home' component={HomeScreen} />
  </Stack.Navigator>
);

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <TabsStackScreen />
        </NavigationContainer>
      </Provider>

    );
  }
}
