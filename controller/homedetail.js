import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, FlatList, Image, 
    Linking, ActivityIndicator, TouchableOpacity
} from 'react-native';
import Constants from 'expo-constants';
import MarqueeText from 'react-native-marquee';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { getDetailDefAction } from './Actions';
import { connect } from 'react-redux'

class HomeDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            country : '',
            source : []
        };
    };
    
    componentDidMount() {
        this.props.getDetailDefAction(1047378);
    };

    componentDidUpdate(prevProps, prevState) {
        if(this.props.status != 200){
            Alert.alert(this.props.msg)
        }
    };

    round = (a) =>{
        return(
            Number(a).toFixed(1)
        )
    };

    round2 = (a) =>{
        return(
            Number(a).toFixed(0)
        )
    };
    
    date = (a) => {
        var x = new Date(a);
        let y = x.toTimeString().split(' ')[0].split(':');
        return(
            y[0]+":"+y[1]
        )
    }

    date2 = (a) => {
        let x = new Date(a).toDateString();
        let z = new Date().toDateString();
        let y ="";
        // console.log(x+" - "+z);

        if (x.split(' ')[2]==z.split(' ')[2]){
            y="Today"
        }else if (x.split(' ')[2]-1==z.split(' ')[2]){
            y="Tomorrow"
        }else{
            y=x.split(' ')[0]+" "+x.split(' ')[2]+" "+x.split(' ')[1]
        }
        
        return(
            y
        )
    }

    marText = (a) => {
        let retKon = "Data Source : ";
        if(a[0]!=null){
            let jal = a;
            for(let x=0; x<jal.length; x++){
                retKon = retKon+jal[x].title+" ("+jal[x].url+") - ";
            }
            
        }
        return(retKon)
    }

    onPressBack = () => {
        this.props.navigation.navigate('LoginScreen');
    };

    render () {
        return (
            this.props.isLoading ? 
                <View style={{flex:1, justifyContent: "center"}}>
                    <ActivityIndicator size="large" />
                </View> 
            :
                <View style={styles.container}>
                    <View style={styles.navBar}>
                        <View style={styles.viewRow}>
                            <Image source={{ uri: 'https://www.metaweather.com/static/img/weather/png/lc.png' }} style={{width:40, height:40, marginLeft:5, marginTop:2}} />
                        </View>
                        <TouchableOpacity onPress={() => this.onPressBack()}>
                            <View style={styles.viewRow}>
                                <Image source={require('../assets/logout.png')} style={{ width: 70, height: 30, marginRight: 5, marginTop:10 }} />
                            </View>
                        </TouchableOpacity>
                        
                    </View>

                    <View style={styles.body}>
                        <View style={styles.viewSpace}>
                            <View style={styles.viewRow}>
                                <Text style={{fontSize:30, color:'#337ab7'}}>{this.props.data.title}</Text>
                                {this.props.data.title != undefined ?
                                    <Text style={{fontSize:15, marginTop:13}}>, {this.props.data.parent.title} </Text>
                                :   
                                    <Text style={{fontSize:15, marginTop:13}}>, {this.state.country} </Text>
                                }
                            </View>
                            <View>
                                <Text style={{fontSize:15, marginTop:13}}>Time {this.date(this.props.data.time)}</Text>
                            </View>
                        </View>
                        <View style={styles.viewSpace}>
                            <Text style={{fontSize:15}}>Sun Rise {this.date(this.props.data.sun_rise)}</Text>
                            <Text style={{fontSize:15}}>Sun Set {this.date(this.props.data.sun_set)}</Text>
                        </View>
                        <FlatList
                            data={this.props.data.consolidated_weather}
                            keyExtractor={(item) => item.id.toString()}
                            renderItem={({item}) =>
                                <View style={styles.viewList}>
                                    <View style={styles.dokList}>
                                        <Text style={styles.textStyleBold}>
                                            {this.date2(`${item.applicable_date}`)}
                                        </Text>
                                        <Image source={{ 
                                            uri: 'https://www.metaweather.com/static/img/weather/png/'+`${item.weather_state_abbr}`+'.png' 
                                            }} style={styles.Image} />
                                    </View>
                                    
                                    <View style={styles.dokList}>
                                        <Text style={styles.textStyleBold}>
                                            {`${item.weather_state_name}`}
                                        </Text>
                                        <Text style={styles.textStyle}>
                                            {"Min: "+this.round(`${item.min_temp}`)+'°C'}
                                        </Text>
                                        <Text style={styles.textStyle}>
                                            {"Max: "+this.round(`${item.max_temp}`)+'°C'}
                                        </Text>
                                        <Text style={styles.textStyle}>
                                            {`${item.wind_direction_compass}`+": "+this.round(`${item.wind_speed}`)+"mph"}
                                        </Text>
                                    </View>

                                    <View style={{width:150}}>
                                        <Text style={styles.textStyle2}>
                                            {"Humidity: "+`${item.humidity}`+'%'}
                                        </Text>
                                        {/* <Text style={styles.textStyle}>
                                            {"Visibility : "+this.round(`${item.visibility}`)+' miles'}
                                        </Text> */}
                                        <Text style={styles.textStyle}>
                                            {"Pressure: "+this.round2(`${item.air_pressure}`)+"mb"}
                                        </Text>
                                        <Text style={styles.textStyle}>
                                            {"Confidence: "+this.round2(`${item.predictability}`)+"%"}
                                        </Text>
                                    </View>
                                </View>
                            }
                        />
                    </View>

                    <View style={styles.tabBar}>
                        <View>
                            <MarqueeText
                                style={styles.textStyleMarq}
                                duration={15000}
                                marqueeOnStart
                                loop
                                marqueeDelay={1000}
                                marqueeResetDelay={1000}
                                >
                                {this.props.data.title != undefined ?
                                    this.marText(this.props.data.sources)
                                :
                                    this.marText(this.state.source)
                                }
                            </MarqueeText>
                        </View>
                        {/* <View style={styles.textBarStyle}>
                            <Text style={{color:'#337ab7'}} 
                                onPress={() => Linking.openURL('https://www.metaweather.com/')}>
                                © www.metaweather.com
                            </Text>
                        </View> */}
                    </View>
                </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: Constants.statusBarHeight
    },
    navBar: {
        flex: 0.09,
        padding:2,
        backgroundColor : '#337ab7',
        flexDirection: 'row',
        justifyContent:'space-between'
    },
    body:{
        flex:1
    },
    viewRow: {
        flexDirection: 'row',
    },
    viewSpace: {
        flexDirection:'row',
        justifyContent:'space-between',
        padding :7,
        borderBottomWidth:1,
    },
    tabBar:{
        backgroundColor : '#f5f5f5',
        height:50,
        borderWidth:0.5,
        borderColor:'#E5E5E5',
        flexDirection:'column',
        justifyContent:'space-around'
    },
    dokList:{
        marginLeft:0, 
        width:115,
    },
    textBarStyle:{
        flexDirection:'row',
        justifyContent:'flex-end'
    },
    textStyle: {
        fontSize : 15,
    },
    textStyle2: {
        fontSize : 15,
        marginTop:22
    },
    textStyleMarq: {
        fontSize: 15, 
        // marginTop:5, 
        color:'#6b6b6b', 
        fontStyle:'italic',
    },
    textStyleBold: {
        fontWeight : 'bold',
        fontSize : 16,
        color:'#337ab7',
        paddingBottom:4,
        // marginTop:20
    },
    viewList: {
        height: 105,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#DDD',
        alignItems: 'stretch',
        padding: 7,
        backgroundColor:'white',
        // margin: 4,
    },
    Image: {
        width: 65,
        height: 65,
        marginLeft:10
    },
    textItemLogin: {
        fontWeight: 'bold',
        textTransform: 'capitalize',
        marginLeft: 20,
        fontSize: 16
    },
    textItemUrl: {
        fontWeight: 'bold',
        marginLeft: 20,
        fontSize: 12,
        marginTop: 10,
        color: 'blue'
    }
});

const mapStateToProp = (state) => ({
    isLoading: state.detaildefsIsLoading,
    data: state.detaildefsData,
    status: state.detaildefsHttpStatus,
    msg: state.detaildefsHttpMsg
});

export default connect(mapStateToProp, { getDetailDefAction })(HomeDetail)