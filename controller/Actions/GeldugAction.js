
import { ambilCityAPI, ambilDetailAPI, ambilDetailDefAPI } from '../API'

export const getCitiesAction = (param) => {
    return (dispatch) => {
        return (ambilCityAPI(dispatch, param));
    }
}

export const getDetailAction = (param) => {
    return (dispatch) => {
        return (ambilDetailAPI(dispatch, param));
    }
}

export const getDetailDefAction = (param) => {
    return (dispatch) => {
        return (ambilDetailDefAPI(dispatch, param));
    }
}

