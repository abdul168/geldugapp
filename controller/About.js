import { StatusBar } from 'expo-status-bar';
import React from 'react';
import {
    StyleSheet, Text, View, FlatList, Image,
    Linking, TextInput, ActivityIndicator, TouchableOpacity, Alert
} from 'react-native';
import Constants from 'expo-constants';
import MarqueeText from 'react-native-marquee';
import { getCitiesAction } from './Actions';

export default class About extends React.Component {

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <View style={styles.navImage}>
                        <View style={{ marginTop: 40}}>
                            <Text style={styles.textStyleBoldItalic}>Back End Developer</Text>
                        </View>
                        <View style={{ marginTop: 40}}>
                            <Text style={styles.textStyleBoldItalic}>Front End Designer </Text>
                        </View>
                    </View>
                    <View style={styles.navImage}>
                        <View style={{ marginTop: 15}}>
                            <Image source={require('../assets/abdul.jpg')} style={styles.Image} />
                        </View>
                        <View style={{ marginTop: 15}}>
                            <Image source={require('../assets/yudin.jpg')} style={styles.Image} />
                        </View>
                    </View>
                    <View style={styles.navImage}>
                        <View style={{ marginTop: 15}}>
                            <Text style={styles.textStyleBold}>Abdullatif</Text>
                        </View>
                        <View style={{ marginTop: 15}}>
                            <Text style={styles.textStyleBold}>Nahyudin</Text>
                        </View>
                    </View>
                    <View style={styles.navImage}>
                        <View style={{ marginTop: 5}}>
                            <Text style={styles.textStyle2}>@abdul168 / @dullatip</Text>
                        </View>
                        <View style={{ marginTop: 5}}>
                            <Text style={styles.textStyle2}>@yudin96 / @nahyudhin</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.body}>
                    <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'center'}}>
                        <Text style={{ fontSize: 20}}>Weather API Source</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center'}}>
                        <Image source={{ uri: 'https://www.metaweather.com/static/img/weather/png/lc.png' }} style={{width:42, height:42, marginRight:10}} />
                        <Text style={{ fontSize: 35, marginTop: 8, color:'#9e9e9e'}}>MetaWeather</Text>
                        <Text style={{ fontSize: 15, marginTop: 26, color:'red'}}>Beta</Text>
                    </View>
                    <View style={{ marginTop: 20, flexDirection: 'row', justifyContent: 'flex-start' }}>
                        <View style={{ marginTop: 15, marginLeft:5}}>
                            <Text style={{ fontSize: 18, marginBottom:5}}>Data Source : </Text>
                            <Text style={styles.textStyle}>BBC (http://www.bbc.co.uk/weather)</Text>
                            <Text style={styles.textStyle}>Forecast.io (http://forecast.io)</Text>
                            <Text style={styles.textStyle}>HAMweather (http://www.hamweather.com)</Text>
                            <Text style={styles.textStyle}>Met Office (http://www.metoffice.gov.uk)</Text>
                            <Text style={styles.textStyle}>OpenWeatherMap (http://openweathermap.org)</Text>
                            <Text style={styles.textStyle}>Weather Underground (https://www.wunderground.com)</Text>
                            <Text style={styles.textStyle}>World Weather (http://www.worldweatheronline.com)</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.tabBar}>
                    <View style={{ marginTop: 5, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text> </Text>
                        <Image source={require('../assets/logo.png')} style={{ width: 150, height: 50, marginRight: 5 }} />
                    </View>
                </View>
            </View>

        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderColor: '#E5E5E5',
        // backgroundColor:'white'
        marginTop: Constants.statusBarHeight
    },
    navBar: {
        flex: 0.8,
        // padding: 2,
        backgroundColor: '#337ab7',
    },
    navImage: {
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    navText: {
        fontSize: 30,
        paddingLeft:10
    },
    body: {
        flex: 1, 
        // flexDirection: 'row', 
        // justifyContent: 'center' 
    },
    viewRow: {
        flexDirection: 'row',
    },
    viewSpace: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 7,
        borderBottomWidth: 1,
    },
    tabBar: {
        // backgroundColor: '#f5f5f5',
        height: 60,
        // borderWidth: 0.5,
        // borderColor: '#E5E5E5',
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    dokList: {
        marginLeft: 0,
        width: 170,
    },
    textBarStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    textStyle: {
        fontSize: 15,
        color: '#8f8f8f',
        fontStyle:'italic'
    },
    textStyle2: {
        fontSize: 15,
        color: '#d7d9d8',
        fontStyle:'italic'
    },
    textStyleBold: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#d7d9d8',
    },
    textStyleBoldItalic: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#d7d9d8',
        fontStyle:'italic'
    },
    viewList: {
        height: 70,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#DDD',
        alignItems: 'center',
        padding: 7,
        backgroundColor: 'white',
        // margin: 4,
    },
    Image: {
        width: 100,
        height: 100,
        borderRadius:100
    },
    textItemLogin: {
        fontWeight: 'bold',
        textTransform: 'capitalize',
        marginLeft: 20,
        fontSize: 16
    },
    textItemUrl: {
        fontWeight: 'bold',
        marginLeft: 20,
        fontSize: 12,
        marginTop: 10,
        color: 'blue'
    },
    input: {
        marginTop: 4,
        marginBottom: 12,
        marginLeft: 5,
        marginRight: 5,
        paddingLeft: 10,
        paddingRight: 10,
        height: 40,
        borderColor: '#337ab7',
        borderWidth: 1,
        borderRadius: 5,
        fontSize: 18,
        backgroundColor: 'white'
    }
});