import { StatusBar } from 'expo-status-bar';
import React from 'react';
import {
    StyleSheet, Text, View, FlatList, Image,
    Linking, TextInput, ActivityIndicator, TouchableOpacity, Alert
} from 'react-native';
import Constants from 'expo-constants';
import MarqueeText from 'react-native-marquee';
import Detail2 from './detail';
import { getCitiesAction } from './Actions';
import { connect } from 'react-redux'

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: false,
        };
    };


    componentDidMount() {
        this.handleSearch('a');
    };

    componentDidUpdate(prevProps, prevState) {
        if(this.props.status != 200){
            Alert.alert(this.props.msg)
        }
    }

    handleSearch = (text) => {
        if(text!=''){
            this.props.getCitiesAction(text)
        }
        
    };

    fetchData = async (a) => {
        if (a != "") {
            const response = await fetch("https://www.metaweather.com/api/location/search/?query=" + a)
                .then((response) => response.json())
                .catch((error) => console.error(error))
                ;
            this.setState({ data: response });
        }
        this.setState({ isLoading: false });

    };

    onPressItem = (a) => {
        this.props.navigation.navigate('Detail', { woeid: a });
    };

    render() {
        const renderItem = ({ item }) => {
            return (
                <Item
                    item={item}
                    onPress={() => this.onPressItem(item.woeid)}
                />
            )
        };

        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <View style={{ marginTop: 5, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text> </Text>
                        <Image source={require('../assets/logo.png')} style={{ width: 130, height: 50, marginRight: 5 }} />
                    </View>
                    <View style={{ marginTop: 10 }}>
                        <View style={styles.viewRow}>
                            <Text style={{ marginLeft: 5, fontWeight: 'bold', color: 'red' }}>* </Text>
                            <Text style={{ fontWeight: 'bold' }}>Search The City On Text Input</Text>
                        </View>
                        <TextInput style={styles.input}
                            underlineColorAndroid="transparent"
                            placeholder="Search"
                            placeholderTextColor="#337ab7"
                            autoCapitalize="none"
                            onChangeText={this.handleSearch} />
                        <View style={styles.viewRow}>
                            <Text style={{ marginLeft: 5, fontWeight: 'bold', color: 'red' }}>*</Text>
                            <Text style={{ marginLeft: 5, fontStyle: 'italic', marginBottom: 5 }}>Tap On List City To Detail View</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.body}>

                    <View>
                        {this.props.isLoading ?
                            <View style={{ flex: 1, marginTop: 40, justifyContent: "center" }}>
                                <ActivityIndicator size="large" />
                            </View>
                            :
                            <TouchableOpacity style={styles.item} onPress={() => this.onPressItem(this.state.data.woeid)}>
                                <FlatList
                                    data={this.props.data}
                                    keyExtractor={(item) => item.woeid.toString()}
                                    renderItem={renderItem}
                                />
                            </TouchableOpacity>
                        }
                    </View>
                </View>
            </View>

        );
    }
};

const Item = ({ item, onPress }) => (
    <TouchableOpacity onPress={onPress} style={[styles.item]}>
        <View style={styles.viewList}>
            <View style={styles.dokList}>
                <Text style={styles.textStyleBold}>
                    {`${item.title}`}
                </Text>
            </View>
            <View style={styles.dokList}>
                <Text style={styles.textStyle}>
                    Latitude, Longitude :
                </Text>
                <Text style={styles.textStyle}>
                    {`${item.latt_long}`}
                </Text>
                <Text style={styles.textStyle}>
                    Woe ID : {`${item.woeid}`}
                </Text>
                {/* <Text style={styles.textStyle}>
                    {`${item.location_type}`}
                </Text> */}
            </View>
        </View>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderColor: '#E5E5E5',
        // backgroundColor:'white'
        marginTop: Constants.statusBarHeight
    },
    navBar: {
        // flex: 2,
        padding: 2,
        backgroundColor: '#f5f5f5',
        // flexDirection: 'row',
        // justifyContent:'flex-end'
    },
    body: {
        flex: 1
    },
    viewRow: {
        flexDirection: 'row',
    },
    viewSpace: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 7,
        borderBottomWidth: 1,
    },
    tabBar: {
        backgroundColor: '#f5f5f5',
        height: 50,
        borderWidth: 0.5,
        borderColor: '#E5E5E5',
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    dokList: {
        marginLeft: 0,
        width: 170,
    },
    textBarStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    textStyle: {
        fontSize: 15,
        // paddingLeft:10
    },
    textStyleBold: {
        fontWeight: 'bold',
        fontSize: 18,
        color: '#337ab7',
        paddingBottom: 5
    },
    viewList: {
        height: 70,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#DDD',
        alignItems: 'center',
        padding: 7,
        backgroundColor: 'white',
        // margin: 4,
    },
    Image: {
        width: 60,
        height: 60,
    },
    textItemLogin: {
        fontWeight: 'bold',
        textTransform: 'capitalize',
        marginLeft: 20,
        fontSize: 16
    },
    textItemUrl: {
        fontWeight: 'bold',
        marginLeft: 20,
        fontSize: 12,
        marginTop: 10,
        color: 'blue'
    },
    input: {
        marginTop: 4,
        marginBottom: 12,
        marginLeft: 5,
        marginRight: 5,
        paddingLeft: 10,
        paddingRight: 10,
        height: 40,
        borderColor: '#337ab7',
        borderWidth: 1,
        borderRadius: 5,
        fontSize: 18,
        color:'#337ab7',
        backgroundColor: 'white'
    },
});

const mapStateToProp = (state) => ({
    isLoading: state.citiesIsLoading,
    data: state.citiesData,
    status: state.citiesHttpStatus,
    msg: state.citiesHttpMsg
});

export default connect(mapStateToProp, { getCitiesAction })(Home)