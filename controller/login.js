import React from "react";
import { FontAwesome5 } from '@expo/vector-icons';

import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Text,
    ScrollView
} from 'react-native';

const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
);
const LoginScreen = ({ navigation }) => (
    // <ScrollView>
        <ScreenContainer>

            <View style={styles.background}>
                <View style={styles.backImage}>
                    <View style={{ alignItems: 'center' }}>
                        <Image source={require('../assets/logo.png')} style={{ marginVertical: 50, width: 250, height: 100 }} />
                    </View>
                </View>

                <View style={{ marginVertical: 40 }}>
                    <View style={styles.flexInput}>
                        <View style={{ marginVertical: 30, alignItems: 'center' }}>
                            <View style={{flexDirection: 'row'}}>
                                <FontAwesome5 name="user-alt" size={24} color="#337ab7" style={{marginTop:7}} />
                                <TextInput style={styles.textInput} placeholder="User Name"/>
                            </View>
                            <Text style={{ marginVertical: 2 }} />
                            <View style={{flexDirection: 'row'}}>
                                <FontAwesome5 name="lock" size={24} color="#337ab7" style={{marginTop:7}} />
                                <TextInput style={styles.textInput} placeholder="Password" secureTextEntry={true} />
                            </View>
                        </View>

                    </View>
                </View>

                <View style={{ alignItems: 'center' }}>
                    <View style={{  height: 300, width: 450, alignItems: 'center' }}>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => navigation.navigate('HomeScreen')}
                        >
                            <Text style={{fontWeight: 'bold', color:'white', fontSize:22}}>Login</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
        </ScreenContainer>
    // </ScrollView>

);
export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: "#337ab7",
        // backgroundColor:'skyblue'
    },
    titleText: {
        fontSize: 30,
        borderColor: '#3e3b63',
        marginVertical: 30,
        justifyContent: 'center',
        textAlign: 'center',
        fontWeight: 'bold',

    },
    background: {
        backgroundColor: '#337ab7',
        // backgroundColor:'skyblue'
    },
    backImage: { 
        backgroundColor: 'white', 
        height:150,
        borderBottomEndRadius:15,
        borderBottomStartRadius:15
    },
    button: {
        height:45,
        alignItems: "center",
        backgroundColor: "#1395d1",
        padding: 7,
        width:150,
        borderRadius:10,
        
    },
    textInput :{ 
        width: 270, 
        height: 40, 
        backgroundColor: 'white', 
        borderRadius: 5, 
        borderWidth: 2, 
        borderColor: '#337ab7',
        marginLeft:5,
        paddingLeft:5 
    },
    flexInput : {
        backgroundColor: 'white', 
        borderRadius: 10, 
        width: 350, 
        alignSelf: 'center', 
        shadowColor: "#000",
        shadowOffset: {
            width: 2,
            height: 12,
        },
        shadowOpacity: 1,
        shadowRadius: 50.00,

        elevation: 24,
    }
})