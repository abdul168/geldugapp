import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Detail from './controller/detail';
import Home from './controller/home';
import About from './controller/About';
import Index from './controller/index';
import Login from './controller/login';


export default function App() {
  return (
    // <Detail/>
    // <Home/>
    <Index/>
    // <Login/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
